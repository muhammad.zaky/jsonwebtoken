const express = require('express');
const router = require('./router');
const passport = require('./lib/passport');

const app = express();

app.use(express.json());
app.use(express.urlencoded({
    extended: false
}))

app.set('view engine', 'ejs')

app.use(passport.initialize());
app.use(router);

app.listen(8000, () => {
    console.log("apps berjalan di port 8000");
})