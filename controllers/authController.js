const { User } = require('../models')

function format(user){
    const {id, username} = user;
    return {
        id,
        username,
        accessToken: user.generateToken()
    }
}

module.exports = {
    register: (req, res, next) => {
        console.log('aaa', req)
        User.register(req.body)
            .then(() => {
                res.json({
                    message: "data sukses tersimpan"
                })
            })
            .catch(err => next(err))
    },
    login: async (req, res) => {
        User.authenticate(req.body)
            .then(user => {
                res.json(
                    format(user)
                )
            })
            .catch(err => res.json({
                message: err
            }))
    },
    whoami: (req, res) => {
        const currentuser = req.user;
        res.json(currentuser);
    }
}