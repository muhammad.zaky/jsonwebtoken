const router = require('express').Router();
const auth = require('./controllers/authController');
const restrict = require('./middleware/restrict');

router.post('/register', auth.register);

router.post('/login', auth.login);

router.get('/whoami', restrict, auth.whoami);

module.exports = router;